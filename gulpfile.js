/**
 * Created by smaudet on 1/25/17.
 */
var gulp = require('gulp')
  , watch = require('gulp-watch')
  , svelte = require('gulp-svelte')
  , tap = require('gulp-tap')
  // , exec = require('child_process').exec
  , runSequence = require('run-sequence')
  // , babel = require('gulp-babel')
  // , webpack = require('gulp-webpack')
  , connect = require('gulp-connect')
  ;

gulp.task('stream', function () {
  // Endless stream mode
  return watch('*.s.html', { ignoreInitial: false })
    .pipe(svelte({
      "format":"iife",
      "name":
    }))
    // .pipe(babel({
    //   presets: ['es2015']
    // }))
    // .pipe(webpack())
    .pipe(gulp.dest('./'))
});

gulp.task('webserver', function() {
  connect.server();
});

gulp.task('default',function () {
  runSequence('webserver',['stream']);
})
