import MyComponent from './mycomponent.s.html';

const component = new MyComponent({
  // `target` is the only required option – the element
  // to render the component to
  target: document.querySelector( 'main' ),

  // `data` is optional. A component can also have
  // default data – we'll learn about that later.
  data: {
    questions: [
      'life',
      'the universe',
      'everything'
    ],
    answer: 42
  }
});